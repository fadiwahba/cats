# Cat List

## Installation and run

1. Clone the git repo: `git clone https://bitbucket.org/fadiwahba/cats.git`
2. Go to the app directory: `cd cats`
3. Install dependencies: `npm install`
4. Run local server: `npm start`
