import { Component } from '@angular/core';
import { DataService } from "./services/data.service";
import { ICat } from "./interfaces/ICat";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // private cats: ICat[];

  constructor(private _dataService: DataService) {
    console.log('AppComponent constructed...');

    // this.cats = this._dataService.cats;
  }
}
