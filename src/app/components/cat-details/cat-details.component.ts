import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ICat } from "../../interfaces/ICat";
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-cat-details',
  templateUrl: './cat-details.component.html',
  styleUrls: ['./cat-details.component.scss']
})
export class CatDetailsComponent implements OnInit {
  @Input() cat: ICat;
  @Output() removeEvent = new EventEmitter();
  @Output() updateEvent = new EventEmitter();

  constructor(private _dataService: DataService) {
    console.log('CatDetailsComponent constructed...');
  }

  ngOnInit() {
    console.log('CatDetailsComponent init...');
  }

  onEditFormSubmit(event) {
    // prepare a new cat object with new updates
    var newId = this.cat.id;
    var newThumbnail = event.target[1].value !== "" ? event.target[1].value : this.cat.thumbnailUrl;
    var newName = event.target[2].value !== "" ? event.target[2].value : this.cat.name;
    var newBirthdate = event.target[3].value !== "" ? event.target[3].value : this.cat.birthdate;
    var newOwnerName = event.target[4].value !== "" ? this.cat.ownername.push(event.target[4].value) : "";

    var newCat: ICat = {
      id: newId,
      thumbnailUrl: newThumbnail,
      name: newName,
      birthdate: newBirthdate,
      ownername: this.cat.ownername,
      viewsCount: this.cat.viewsCount
    }

    // apply changes
    this._dataService.editDataById(this.cat.id, newCat);

    // update the view
    this.updateEvent.emit(this.cat.id);
  }

  onConfirmDelete(id: number) {
    this.removeEvent.emit(id);
  }

}
