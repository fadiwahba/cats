import { Component, OnInit } from '@angular/core';
import { NgClass } from '@angular/common';
import { DataService } from "../../services/data.service";
import { ICat } from "../../interfaces/ICat";

@Component({
  selector: 'app-cat-list',
  templateUrl: './cat-list.component.html',
  styleUrls: ['./cat-list.component.scss']
})
export class CatListComponent implements OnInit {

  cats: ICat[] = [];
  selectedCat: ICat;
  showError: boolean;
  term: any;

  constructor(private _dataService: DataService) {
    console.log('CatListComponent constructed...');
    this.showError = false;
  }

  ngOnInit() {
    console.log('CatListComponent init...');
    this.cats = this._dataService.cats;
    this.selectedCat = this.cats[0];
  }

  onSelect(event, cat: ICat): void {
    event.preventDefault();
    cat.viewsCount ++;
    this.selectedCat = cat;
    this._dataService.saveToLocalStorage(this.cats);
  }

  onUpdateEvent(id) {
    this.cats = this._dataService.cats;
    this.selectedCat = this.findCatById(id);
  }

  onRemoveEvent(id) {
    this.cats = this._dataService.cats;
    this._dataService.deleteDataById(id);
    this.cats = this._dataService.cats;
    this.selectedCat = this.cats[0];
    if (this.cats.length === 0) {
      this.showError = true;
    }
  }

  findCatById(id: number): ICat {
    return this.cats.find(function (cat) {
      return cat.id === id;
    });
  }
}
