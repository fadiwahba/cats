export interface ICat {
  id: number,
  thumbnailUrl: string,
  name: string,
  birthdate: string, // YYYY-MM-DD
  ownername: string[],
  viewsCount: number
}
