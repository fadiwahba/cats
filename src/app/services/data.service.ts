import { Injectable } from '@angular/core';
import { ICat } from 'app/interfaces/ICat';

@Injectable()
export class DataService {

  // cats array data
  cats: ICat[];

  constructor() {
    console.log('data service started...');

    var initialData = [
      {
        id: 1,
        thumbnailUrl: '/assets/images/cats/cat01.jpeg',
        name: 'Ashes',
        birthdate: '2015-01-15',
        ownername: ['tommy', 'alice', 'mike'],
        viewsCount: 0
      },
      {
        id: 2,
        thumbnailUrl: '/assets/images/cats/cat02.jpeg',
        name: 'Tiger',
        birthdate: '2015-02-11',
        ownername: ['Dino', 'Raphael', 'Thomas'],
        viewsCount: 0
      },
      {
        id: 3,
        thumbnailUrl: '/assets/images/cats/cat03.jpeg',
        name: 'Smokey',
        birthdate: '2015-03-23',
        ownername: ['Mathias', 'Jocelyn', 'Peter'],
        viewsCount: 0
      },
      {
        id: 4,
        thumbnailUrl: '/assets/images/cats/cat04.jpeg',
        name: 'Kitty',
        birthdate: '2013-12-03',
        ownername: ['Daniel', 'Ivan', 'Carlos'],
        viewsCount: 0
      },
      {
        id: 5,
        thumbnailUrl: '/assets/images/cats/cat05.jpeg',
        name: 'Oscar',
        birthdate: '2013-07-16',
        ownername: ['Michael', 'Debbie', 'Angelina'],
        viewsCount: 0
      },
    ];

    // test whether the local storage contains data or not, if empty then populate it
    if (localStorage.getItem('cats') == null) {
      this.populateStorage(JSON.stringify(initialData));
      console.log('LocalStorage populated');
    }

    // initialize cats array
    this.cats = JSON.parse(this.loadAllData('cats'));
    console.log('Cats: \n', this.cats);

  }

  // @param data: string data to be stored
  // return void
  // this method populate localStorage with initial data
  populateStorage(data: string): void {
    localStorage.setItem('cats', data);
  }

  // @param data: data to be stored in a local storage
  // return void
  // this method saves data in a local Storage
  saveToLocalStorage(data: any): void {
    if (data) {
      localStorage.setItem('cats', JSON.stringify(data));
    }
  }

  // fetches data from local staorage by a key
  loadAllData(key: string): string {
    if (localStorage.getItem(key)) {
      return localStorage.getItem(key);
    } else {
      return '';
    }
  }

  // @param id: id of cat to be deleted
  // return void
  // removes a cat from the list by ID
  deleteDataById(id: number): void {
    if (this.cats.length > 0) {
      this.cats = this.cats.filter(function (item) {
        return item.id !== id;
      });
    }
    // save updated list to local storage
    this.saveToLocalStorage(this.cats);
  }

  // @param id: id of cat to be updated
  // @param newCat: new cat's values
  // return void
  // update a cat from the list by ID
  editDataById(id: number, newCat: ICat) {
    if (this.cats.length > 0) {
      var cat = this.cats.find(function (cat) {
        return cat.id === id
      });

      if (cat) {
        cat.id = newCat.id;
        cat.thumbnailUrl = newCat.thumbnailUrl;
        cat.name = newCat.name;
        cat.birthdate = newCat.birthdate;
        cat.ownername = newCat.ownername;
        cat.viewsCount = newCat.viewsCount;
      }
    }
    // save updated list to local storage
    this.saveToLocalStorage(this.cats);
  }

}
